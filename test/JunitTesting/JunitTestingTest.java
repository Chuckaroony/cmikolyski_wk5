package JunitTesting;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;


import static org.junit.jupiter.api.Assertions.*;

class JunitTestingTest {
    JunitTesting unitToTest = new JunitTesting();


    //This Junit uses the assertEquals method. AssertEquals compares an expected result with an
    //actual result from the method being tested.
    @Test
    public void TestAssertEquals(){

        int result = unitToTest.AddNumber(3, 4);
        int expected = 7;
        assertEquals(expected,result);

    }

    //Assert true test checks to see if a logical condition is true.
    //In my code below I test if the result from my random number creator method
    //is greater than or equal to one. That way I can test if the random number being created is
    //really in the range specified in the parameters.
    @Test
    public void TestAssertTrue(){
        int trueRandom = unitToTest.RandomNumber(1, 10);
        assertTrue(trueRandom >= 1);
    }

    //Assert false is the inverse of assert true. It can be used to
    //test if a logical condition is false. In the test below I check to
    //see if the number created from the random number method is greater than
    //the range set within my parameters. If it is not greater than 10, it passes test.
    @Test
    public void TestAssertFalse(){
        int falseRandom = unitToTest.RandomNumber(1, 10);
        assertFalse(falseRandom > 10);
    }

    //Here I put both assert true and assert false together to check that the
    //random number generator method does not create numbers out of range.
    @Test
    public void checkRandomRange(){
        int rando = unitToTest.RandomNumber(1,10);
        assertTrue(rando >= 1);
        assertFalse(rando > 10);
    }

    //Assert same checks to see if two object references point to the same object.
    //In the below test I check to see if my test map contains the same string literal
    // as another map with the same original key and value mapping. However, the
    // key value mapping for Banana has been changed from 1 to 2 and the test map is checked against
    //a different value of another map.
    @Test
    public void TestAssertSame(){
        Map testMap = unitToTest.createMap();
        Map anotherMap = unitToTest.createMap();
        anotherMap.put(1, "Banana");
        assertSame(testMap.get(2), anotherMap.get(1));

    }

    //Assert null checks to see if an object is null.
    //I use the createServer method from last weeks assignment. I provide
    //an incorrect parameter for the url, and it returns a null server which passes test. An exception
    // from the method is also caught and displayed, so I know the exception handling is working.
    @Test
    public void TestAssertNull() throws IOException {
        HttpServer server = unitToTest.createServer("localhoot", 8000 );
        assertNull(server);
    }

    //Assert not null is the inverse of assert null. It checks to see that an object is not null.
    // Below the same createServer method is used, but with a proper url address provided.
    // Since the server is successfully created the test is successful.
    @Test
    public void TestAsserNotNull() throws IOException {
        HttpServer server = unitToTest.createServer("localhost", 8000);
        assertNotNull(server);

    }

    //Assert array equals checks if two arrays contain the same thing.
    //Below I am comparing the testArray, with an expected array.
    //With this test method we are not comparing references. We are comparing the actual content
    //of the array. Since the arrays contain the same thing, the test passes.
    @Test
    public void TestAssertArrayEquals(){
        String[] testArray = unitToTest.CreateArray();
        String[] expected = unitToTest.CreateArray();
        assertArrayEquals(expected, testArray);
    }
}