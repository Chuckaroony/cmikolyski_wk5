package JunitTesting;

import com.sun.net.httpserver.*;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.InetSocketAddress;
import java.util.*;


public class JunitTesting {


    //This method adds two integers together.
    public static int AddNumber(int num1, int num2) {
        return num1+num2;
    }

    //This method creates a random number within a specific range that is passed as parameters.
    public static int RandomNumber(int min, int max){
        int randoInt = (int) (Math.random()*(max-min+1) + min);
        return randoInt;
    }

    //This method creates a server object. It returns a null object if it fails to create the server.
    public static HttpServer createServer(String url, int port) throws IOException {
        HttpServer server =null;
        try{

            server = HttpServer.create(new InetSocketAddress(url, port), 0);

        } catch (IOException io) {
            System.err.println(io.toString()+ " There is a problem with the url address or port number.");
        }

        return server;
    }


    // This method creates a hash map and adds value and key pairs.
    public static Map createMap(){
        Map theMap = new HashMap();
        theMap.put(1, "Mango");
        theMap.put(2, "Banana");
        theMap.put(3, "Aracaju");
        theMap.put(4, "Abacaxi");
        theMap.put(5, "Guava");
        return theMap;
    }

    //this method creates an array filled with fun fruit.
    public static String[] CreateArray(){
        String[] testArray = {"Mango", "Banana", "Aracaju", "Abacaxi", "Guava"};
        return testArray;
    }

    //This main method is for quick testing of my methods functionality.
    public static void main(String[] args) {
        int rando = RandomNumber(0, 4);
        System.out.println(rando);
        String[] printArray = CreateArray();
        System.out.println(printArray[rando]);
    }
}

